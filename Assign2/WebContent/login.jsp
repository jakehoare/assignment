<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>
<script src="js/loginValidate.js"></script>

<title>Login Page</title>
</head>
<body>
<jsp:include page="/header.jsp"/>

<div class="container">
<h2>Please sign in</h2>


<c:choose>
	<c:when test="${not empty param.failed}">
		<h4 style="color: red;"><span class="glyphicon glyphicon-exclamation-sign"></span> Your username or password is incorrect. Or your email address is not confirmed.</h4>
	</c:when>
</c:choose>

<c:choose>
	<c:when test="${not empty param.name}">
		<h4 style="color: #4ab025;"><span class="glyphicon glyphicon-ok-sign"></span> Hi ${param.name}! Your account is successfully activated, please sign in.</h4>
	</c:when>
</c:choose>


    	<div class="row">
			<div class="col-sm-6">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<a href="#" class="active" id="login-form-link">Login</a>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="dispatcher?operation=login" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" required autofocus>
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group text-center">
										<hr>
										<label for="remember"> </label>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<button type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login">Submit</button>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="#" tabindex="5" class="forgot-password"></a>
												</div>
											</div>
										</div>
									</div>
								</form>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="panel panel-login">
					<br>
					<br>
					<br>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="register-form" action="register.jsp" method="post" role="form" style="display: block;">
								<div class="form-group text-center">
									<label for="remember"><h3>New to Square Root?</h3></label>
									<br>
									<br>
									<label for="remember"><h5>Get started now. It's fast and easy!</h5></label>
								</div>
								<div class="form-group">
									<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit"  tabindex="4" class="form-control btn btn-register" value="Register" >
											</div>
									</div>
								</div>
								</form>
								<br>
								<br>
								<br>		
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>
</div>


<jsp:include page="/footer.jsp"/>
</body>
</html>
