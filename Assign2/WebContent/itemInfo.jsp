<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="edu.unsw.comp9321.hibernateBeans.ItemBean"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
   <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<script src="http://code.jquery.com/jquery-latest.js"></script>
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>Item Info Page</title>

<script>
  $(function(){
	    $('.addButtons').click(function(){
	    	
	    		var id = $(this).attr("id");
	    		$.post('dispatcher?operation=addwish',{addWishID:id, addWishByUser:$('#username').val()},function(responseText) { 

	    			$('#'+id).addClass('disabled');  
           });

	      });
	 });
</script>

</head>
<body>

<jsp:include page="/header.jsp"/>

<iframe name="hiddenFrame" class="hide"></iframe>

<pre><font size="1"><i class="glyphicon glyphicon-chevron-right"></i> <b>Listed in category:  ${itemInfo.category}</b></font></pre>
<br>


<c:set var="string1" value="${itemInfo.currency}" />
<c:choose>
	<c:when test="${string1 == 'AUD'}">
		<c:set var="cur" value="$" />
	</c:when>
	<c:when test="${string1 == 'USD'}">
		<c:set var="cur" value="$" />
	</c:when>
	<c:when test="${string1 == 'GBP'}">
		<c:set var="cur" value="£" />
	</c:when>
	<c:otherwise>
		<c:set var="cur" value="€" />
	</c:otherwise>
</c:choose>

<fmt:parseNumber var="num1" type="number" value="${itemInfo.bestBid}" />
<fmt:parseNumber var="num2" type="number" value="${itemInfo.bidIncrements}" />
<c:set var="minBid" value="${num1+num2}"/>

<div class="container-fluid">
<div class="container1">
  	<div class="flex1">
  		<div class="row">
  			 <a href="#" class="thumbnail">
      			<img src="${itemInfo.picture}" alt="${itemInfo.title}">
    		</a>
		</div>  		
	</div>
	<div class="none">
	</div>
  	<div class="flex2">
  		<div class="panel2 panel-default">
			<div class="panel-heading">
				<h3>${itemInfo.title}</h3>
			</div>
			
			<table class="table borderless">
    			<tbody>
			      <tr>
			        <td>Description:</td>
			        <td>${itemInfo.description}</td>
			      </tr>
			      <tr>
			        <td>End time:</td>
			        <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${itemInfo.endTime}" /></td>
			      </tr>
			    </tbody>
  			</table>
  			
  			<table class="table borderless2">
  				<tbody>
  				  <tr>
			      	<td>Current bid:</td>
			      	<td><b>${itemInfo.currency} ${cur}${itemInfo.bestBid}</b></td>
			      	<td></td>
			      </tr>
			      <tr>
			      	<td></td>
			      	<td>
			      		<c:choose>
							<c:when test="${empty user}">
								<form action="login.jsp" method="post" role="form" id="bid-form">
		                			<button class="btn btn-primary btn-lg" type="submit">Sign in to Bid</button>
	                			</form>
							</c:when>
			      			<c:otherwise>
			      				<c:choose>
									<c:when test="${itemInfo.itemStatus == 'LIVE'}">
										<form action="dispatcher?operation=bid" method="post" role="form" id="bid-form">
								        	<div class="form-group">
								        	<input type="hidden" name="ID" value="${itemInfo.itemId}"/>
								        	<input type="hidden" name="previousBid" value="${itemInfo.bestBid}"/>
											<input type="number" class="form-control" name="bid" style="width: 150px;" step="0.01" max="999999" required/>
						                	</div>
						                	<button class="btn btn-primary btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Place bid&nbsp;&nbsp;&nbsp;&nbsp;</button>
					                	</form>
									</c:when>
			      					<c:otherwise>
			      						<form action="#" method="post" >
				                			<button disabled class="btn btn-primary btn-lg" type="submit">Item ${itemInfo.itemStatus}</button>
			                			</form>
			      					</c:otherwise>
			      				</c:choose>
			      			</c:otherwise>
			      		</c:choose>
			        </td>
			        <td></td>
			      </tr>
			      <tr>
			      	<td>Bid increments:</td>
			      	<td><b>${itemInfo.currency} ${cur}${itemInfo.bidIncrements}</b><br>
			      		<font size="1"><b>Enter ${itemInfo.currency} ${cur}
			      			<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${minBid}" />
			      			or more</b></font>
			      	</td>
			      	<td></td>
			      </tr>
			      <tr>
			      	<td></td>
			      	<td></td>
			      	<td>
			      	<c:choose>
						<c:when test="${empty user}">
		               		<a href='login.jsp'><button style="background:#e0e0e0; border:0;" class="btn btn-default btn-sm hvr-fade2" type="submit"><span class="glyphicon glyphicon-heart"></span><b>Add to Wish List</b></button></a>
						</c:when>
						<c:otherwise>
								<c:set var="contains" value="false" />
								<c:forEach var="curItem" items="${wishlist}">
								  <c:if test="${curItem.itemId eq itemInfo.itemId}">
								    <c:set var="contains" value="true" />
								  </c:if>
								</c:forEach>
					
							 	<c:choose>
							 	<c:when test="${contains == 'true'}">		
							 			<input type="hidden" id="username" value="${user.username}" />
							 			<button id="${itemInfo.itemId}" style="background:#e0e0e0; border:0;" class="addButtons btn btn-default btn-sm hvr-fade2" type="submit" disabled><span class="glyphicon glyphicon-heart"></span><b>Add to Wish List</b></button>
								</c:when>
								<c:otherwise>
									<input type="hidden" id="username" value="${user.username}" />
							 			<button id="${itemInfo.itemId}" style="background:#e0e0e0; border:0;" class="addButtons btn btn-default btn-sm hvr-fade2" type="submit"><span class="glyphicon glyphicon-heart"></span><b>Add to Wish List</b></button>
								</c:otherwise>
								</c:choose>
						</c:otherwise>
					</c:choose>
			      	</td>
			      </tr>
  				</tbody>
  			</table>
  			<br>
  			<table class="table border3" >
    			<tbody>
			      <tr>
			        <td><b>Seller information</b></td>
			        <td></td>
			      </tr>
			      <tr>
			      	<td>Seller:</td>
			      	<td>${itemInfo.seller.username}</td>
			      </tr>
			      <tr>
			       <td>Postal address:</td>
			       <td>
			       	${itemInfo.seller.addressStreet} <br> 
					${itemInfo.seller.addressCity}&nbsp;&nbsp;${itemInfo.seller.addressState}<br>
					${itemInfo.seller.addressPostcode}&nbsp;&nbsp;${itemInfo.seller.addressCountry} <br>
			       </td>
			      </tr>
			    </tbody>
  			</table>
		</div>
	</div>
</div>
</div>

<jsp:include page="/footer.jsp"/>

</body>
</html>
