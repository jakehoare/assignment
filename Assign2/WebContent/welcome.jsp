<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>Home Page</title>

</head>
<body>

<jsp:include page="/header.jsp"/>

<div class="container">
<br>
<iframe name="hiddenFrame" class="hide"></iframe>
<% int i = 0; %>
<div class="imgBox">
<c:forEach var="item" items="${selectedItems}">
		<div class="img">
	 	<img src="${item.picture}" alt="${item.title}" width="220" height="280">
	 	<div class="desc">
	 		<form action="dispatcher?operation=itemInfo" method="POST" id="${item.itemId}" name="${item.itemId}">
	 			<input type="hidden" name="itemID" value="${item.itemId}" />
           		<button style="width: 230px;height:auto;"  type="submit" class="hvr-fade">${item.title}</button>
           	</form>
	 	</div>
	 	<form action="#" method="POST" target="hiddenFrame">
		</form></div>
		<% i++; 
		if((i % 4) == 0) { %>
			<br class="clearboth">
		<% } %>
</c:forEach>
</div>
</div>

<jsp:include page="/footer.jsp"/>

</body>
</html>