<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>
<script src="js/registerValidate.js"></script>

<title>Register Page</title>

</head>
<body>

<jsp:include page="/header.jsp"/>



		
	<div class='container-fluid'>
	<br><br>
	
<c:choose>
	<c:when test="${not empty param.failed}">
		<h4 style="color: red;"><span class="glyphicon glyphicon-exclamation-sign"></span> Registration failed. Your username or email address is already registered with Square Root.</h4>
	</c:when>
</c:choose>

	<br>
	
	<div class='col-sm-2'></div>
	
	<div class='col-sm-8 text-center center-block' style='border: 1px #e4e4e4 solid;'>
	<h1 style='font-weight: 100;letter-spacing: 2px'> Register </h1>
	<hr>
	
	<form class="form-horizontal" action='dispatcher?operation=register' method='post'>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='usernameLabel' for="username" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Username*</label>
	    <div class="col-sm-6">
	      <input type="text" name='username' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="username" placeholder="username (alphanumeric and underscore)" maxlength="20" pattern="[^\W]+" required autofocus/>
	    </div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='passwordLabel' for="password" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Password* </label>
	    <div class="col-sm-6">
	      <input type="password" name='password' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="password" placeholder="password (no space)" maxlength="64" pattern="[^\t\s\r\n]+" required/>
	    </div>
	</div>
		
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='emailLabel' for="email" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Email* </label>
	    <div class="col-sm-8">
	      <input type="email" name='email' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="email" placeholder="valid email address" maxlength="255" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" required/>
	    </div>
	</div>
		
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='nicknameLabel' for="nickname" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Nickname </label>
	    <div class="col-sm-8">
	      <input type="text" name='nickname' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="nickname" placeholder="tell us what people call you (alphanumeric and underscore)" maxlength="255" pattern="[^\W]+"/>
	    </div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='firstNameLabel' for="firstName" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Name </label>
	    <div class="col-sm-6">
	      <input type="text" name='firstName' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="firstName" placeholder="first name" maxlength="255" />
	    </div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='lastNameLabel' for="lastName" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'> </label>
	    <div class="col-sm-6">
	      <input type="text" name='lastName' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="lastName" placeholder="last name" maxlength="255" />
	    </div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='dateOfBirth' for="dateOfBirth" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Date of Birth </label>
	    <div class="col-sm-6">
	      <input type="date" name='dateOfBirth' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="dob" placeholder="date of birth (yyyy-mm-dd)" />
	    </div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" for="addressStreet" id='addressStreetLabel' style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Postal Address </label>
	    <div class="col-sm-8">
	      <input type="text" name='addressStreet' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="addressStreet" placeholder=" street " maxlength="255" >
	    </div>	    
	</div>
	
	<div class='col-sm-12'>
	  	<div class='col-sm-3'></div>
	  	<div class='col-sm-4' style='margin-left:0;padding-left:0'>
	  		<input type="text" name='addressCity' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="addressCity" placeholder="city" maxlength="255" >
	  	</div>
	  	<div class='col-sm-4'>
	  		<input type="text" name='addressState' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px;width:95%' id="addressState" placeholder="state" maxlength="255" >
	  	</div>
	</div>
	
	<div class='col-sm-12' style='margin-top:10px;margin-bottom:15px'>
	  	<div class='col-sm-3'></div>
	  	<div class='col-sm-4' style='margin-left:0;padding-left:0'>
	  		<select class="form-control input-sm" name='addressCountry'> 
	  			<option value="" selected="selected">Country</option>
	  			<jsp:include page="/country.jsp"/>
	  		</select>
	  	</div>
	  	<div class='col-sm-4'>
	  		<input type="text" name='addressPostcode' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px;width:95%' id="addressPostcode" placeholder="postcode" maxlength="20" >
	  	</div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='creditCardLabel' for="creditCard" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Credit Card </label>
	    <div class="col-sm-6">
	      <input type="text" name='creditCard' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="creditCard" placeholder="16 digit card number" maxlength="16" pattern="\d{16}"/>
	    </div>
	</div>
	
	<div class='col-sm-12 text-center' style='margin-bottom:10px;margin-top:10px'>
    	<button type="submit" value='register' class="btn btn-default btn-sm" style='border-radius:0;letter-spacing: 1px'><span class='text-primary'>Submit</span></button>    	    	
      </div>
	
	</form>
	
	<div class='col-sm-12 text-left' style='margin-bottom:10px;margin-top:10px'>
		Fields marked with * are required.
	</div>
	
	</div>
	
	<div class='col-sm-2'></div>
	
	</div>
	
<jsp:include page="/footer.jsp"/>
</body>
</html>
