<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="edu.unsw.comp9321.hibernateBeans.UserBean"%>
<%@page import="edu.unsw.comp9321.hibernateBeans.MessageBean"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">

<link rel="stylesheet" type="text/css" href="css/dataTables.css">
<script src="js/tablesorter.js"></script>
<script>
$(document).ready(function() {
    $('#mytable').dataTable( {
        "pagingType": "full_numbers", 
        "order": [[ 1, "desc" ]]
    } );
} );
</script>

<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>My Messages Page</title>
</head>
<body>
<jsp:include page="/header.jsp"/>

<br>


<div class="container">
<div class="page-header">
<h2>My Messages</h2>
</div>

<c:choose>
	<c:when test="${empty user}">
	 	<h4>Please login to see your message.</h4>
	</c:when>
	<c:when test="${empty messages}">
	 	<h4>You have no message.</h4>
	</c:when>
	<c:otherwise>

		<table id="mytable" class="table">
		<thead>
		<tr>
			<th>Subject</th>
			<th>Time</th>
		</tr>
		</thead>
		<tbody>
		<%
		List<MessageBean> messages = (List<MessageBean>)request.getAttribute("messages");
		for (MessageBean message : messages) {
		%>
		<tr>
			<td>
				<div><b><%= message.getSubject() %></b></div>
				<br>
				<div><%= message.getContent() %></div>
			</td>
			<td><%= message.getInsertTime() %></td>
		</tr>
		<%
		}
		%>
		</tbody>
		</table>	

</c:otherwise>
</c:choose>
</div>





<jsp:include page="/footer.jsp"/>

</body>
</html>