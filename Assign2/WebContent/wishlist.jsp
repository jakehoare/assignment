<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="edu.unsw.comp9321.hibernateBeans.UserBean"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">

<link rel="stylesheet" type="text/css" href="css/dataTables.css">
<script src="js/tablesorter.js"></script>

<script>
  $(function(){
	    $('.deleteButtons').click(function(){
	    	
	    		var id = $(this).attr("id");
	    		$.post('dispatcher?operation=deletewish',{deleteWishID:id, deleteWishByUser:$('#username').val()},function(responseText) { 
            	$('#row'+id).remove();  
            	location.reload();
           });
	      });
	    $(document).ready(function() {
	        $('#mytable').dataTable( {
	            "pagingType": "full_numbers"
	        } );
	    } );
	 });
</script>

<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>Wish List Page</title>
</head>
<body>
<jsp:include page="/header.jsp"/>

<br>


<div class="container">
<div class="page-header">
<h2>Wish List</h2>
</div>

<c:choose>
	<c:when test="${empty wishlist}">
	 	<h4>The wish list is empty.</h4>
	</c:when>
	<c:otherwise>
		<input type="hidden" id="username" value="${user.username}" />
	</c:otherwise>
</c:choose>



<table id="mytable" class="table">
<thead>
<tr><th></th>
	<th>Title</th>
	<th>Price</th>
	<th>EndTime</th>
	<th>Actions</th>
</tr>
</thead>
<tbody>
<c:forEach var="item" items="${wishlist}">
<tr id="row${item.itemId}">
	<td><img src="${item.picture}" alt="${item.title}" width="220" height="280"></td>
	<td>${item.title}</td>
		<c:set var="string1" value="${item.currency}" />
		<c:choose>
			<c:when test="${string1 == 'AUD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'USD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'GBP'}">
				<c:set var="cur" value="£" />
			</c:when>
			<c:otherwise>
				<c:set var="cur" value="€" />
			</c:otherwise>
		</c:choose>
	<td>${string1} <br>
		Best Bid: ${cur}${item.bestBid}<br>
		Bidding Increments: ${cur}${item.bidIncrements}
	</td>
	<td>End Time: ${item.endTime}<br>
		Status: ${item.itemStatus}</td>
	<td>
		<c:choose>
			<c:when test="${item.itemStatus == 'LIVE'}">
			 	<form action="dispatcher?operation=itemInfo" method="POST" id="${item.title}" name="${item.title}">
					<input type="hidden" name="itemID" value="${item.itemId}" />
					<input type="submit" class="btn btn-default" value="Bid Now"  />
				</form>
			</c:when>
			<c:otherwise>
				<form action="dispatcher?operation=itemInfo" method="POST" id="${item.title}" name="${item.title}" >
					<input type="hidden" name="itemID" value="${item.itemId}" />
					<input type="submit" class="btn btn-default" value="Show Info"  />
				</form>
			</c:otherwise>
		</c:choose>
		<br>
	 	<!-- <iframe name="hiddenFrame" class="hide"></iframe> -->

		<button type="submit" id="${item.itemId}" class="deleteButtons btn btn-default">Remove from Wish List</button>

	</td>
</c:forEach>
</tbody>
</table>

</div>


<jsp:include page="/footer.jsp"/>

</body>
</html>