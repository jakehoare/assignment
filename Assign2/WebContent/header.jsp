<%@page import="edu.unsw.comp9321.hibernateBeans.ItemBean"%>
<%@page import="edu.unsw.comp9321.hibernateDao.support.UserDAOImpl"%>
<%@page import="edu.unsw.comp9321.hibernateBeans.UserBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<body>	
<%
	UserBean user = (UserBean)session.getAttribute("user");
		if(user == null){
%>
<nav class="navbar navbar-default navbar-fixed-top" >
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="forward.jsp"><img style="margin:0px auto;display:block" src="images/logo6.png" alt="Square Root - Online Auction" width="180" height="30"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="register.jsp"><span class="glyphicon glyphicon-user"></span> Register</a></li>
        <li><a href="login.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
    
  </div>
</nav>

<br>
<h1><a href="forward.jsp"><img style="margin:0px auto;display:block" src="images/logo4.png" alt="Square Root - Online Auction"></a></h1>


<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="forward.jsp">Welcome</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar1">
   		<div class="pull-left">
   			<form class="navbar-form" role="search" action="dispatcher?operation=search" method="POST">
        		<div class="input-group">
        			<select class="form-control" style="width: 100px;" name="category">
								<option value="All">All</option>
							  <option value="Arts">Arts</option>
							  <option value="Books">Books</option>
							  <option value="Clothing">Clothing</option>
							  <option value="Computers">Computers</option>
							  <option value="Electronics">Electronics</option>
							  <option value="Games">Games</option>
							  <option value="Shoes">Shoes</option>
							  <option value="Sports">Sports</option>
							</select>
            		<input type="text" class="form-control" placeholder="Search..." name="keywords" id="srch-term" style="width: 500px;" >
            		<div class="input-group-btn">
                		<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            		</div>
        		</div>
        	</form>
       </div>
       
	   <ul class="nav navbar-nav">
	   		<li><a href="advancedSearch.jsp">Advanced Search</a></li>
	   </ul>
    </div>
  </div>
</nav>
<%
	} else {
%>

<% if(!user.getIsAdmin()) { %>
<nav class="navbar navbar-default navbar-fixed-top" >
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="forward.jsp"><img style="margin:0px auto;display:block" src="images/logo6.png" alt="Square Root - Online Auction" width="180" height="30"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
      	<li><a > Hi <b>${user.username}</b>!</a></li>
      	 <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> My Profile<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="forward3.jsp"><span class="glyphicon glyphicon-star"></span> My Collections</a></li>
            <li><a href="dispatcher?operation=listmessages"><span class="glyphicon glyphicon-envelope"></span> My Messages</a></li>
            <li><a href="profile.jsp"><span class="glyphicon glyphicon-edit"></span> Account Settings</a></li>
            <li><a href="logout.jsp"><span class="glyphicon glyphicon-off"></span> Sign Out</a></li>
          </ul>
        </li>
        <li><a href="forward2.jsp"><span class="glyphicon glyphicon-heart"></span> Wish List</a></li>
         <li><a href="additem.jsp"><span class="glyphicon glyphicon-plus"></span> Add Item</a></li>
      </ul>
    </div>
    
  </div>
</nav>

<br>
<h1><a href="forward.jsp"><img style="margin:0px auto;display:block" src="images/logo4.png" alt="Square Root - Online Auction"></a></h1>





<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="forward.jsp">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar1">
   		<div class="pull-left">
        	<form class="navbar-form" role="search" action="dispatcher?operation=search" method="POST">
        		<div class="input-group">
        			<select class="form-control" style="width: 100px;" name="category">
							  <option value="All">All</option>
							  <option value="Arts">Arts</option>
							  <option value="Books">Books</option>
							  <option value="Clothing">Clothing</option>
							  <option value="Computers">Computers</option>
							  <option value="Electronics">Electronics</option>
							  <option value="Games">Games</option>
							  <option value="Shoes">Shoes</option>
							  <option value="Sports">Sports</option>
							</select>
            		<input type="text" class="form-control" placeholder="Search..." name="keywords" id="srch-term" style="width: 500px;" >
            		<div class="input-group-btn">
                		<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            		</div>
        		</div>
        	</form>
       </div>
       
	   <ul class="nav navbar-nav">
	   		<li><a href="advancedSearch.jsp">Advanced Search</a></li>
	   </ul>
    </div>
  </div>
</nav>
<% } else { %>
	<jsp:include page="/adminHeader.jsp"/>
<% } }
%>

</body>
</html>
