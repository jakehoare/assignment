<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="edu.unsw.comp9321.hibernateBeans.ItemBean"%>
<%@page import="edu.unsw.comp9321.hibernateDao.support.UserDAOImpl"%>
<%@page import="edu.unsw.comp9321.hibernateBeans.UserBean"%>
<%@page import="edu.unsw.comp9321.Util"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>
<script src="js/registerValidate.js"></script>

<title>Profile Page</title>

</head>
<body>

<jsp:include page="/header.jsp"/>

		
	<div class='container-fluid'>
	<br><br>

	<div class='col-sm-2'></div>
	
	<div class='col-sm-8 text-center center-block' style='border: 1px #e4e4e4 solid;'>
	<%
	UserBean user = (UserBean)session.getAttribute("user");
	String state = (String)request.getParameter("state");
		if(user != null){
			if(state == null || state.equals("updated")) {
				
			
	%>
	<h1 style='font-weight: 100;letter-spacing: 2px'> Your Profile </h1>
	<hr>
	
	<% 	String state1 = (String)request.getParameter("state");
		if(state1 != null) {
			if(state1.equals("updated")){ %>
				<h4 style="color: #4ab025;"><span class="glyphicon glyphicon-ok-sign"></span> Successfully updated your profile.</h4>
	<%} }  %>
	
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Username :  
	    </span>
	    <span class="col-sm-8 text-left text-primary" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	<%= user.getUsername() %>  
	    </span>
	</div>
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Email :  
	    </span>
	    <span class="col-sm-8 text-left" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	<%= user.getEmail() %>  
	    </span>
	</div>
	
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Nickname :  
	    </span>
	    <c:choose>
		<c:when test="${(not empty user.nickname) && (user.nickname ne 'null') && (user.nickname ne 'NULL') && (user.nickname ne 'Null')}">
		    <span class="col-sm-8 text-left" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
		    	<%= user.getNickname() %> 
		    </span>
		</c:when>
		</c:choose>
	</div>
	
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Name :  
	    </span>
		    <span class="col-sm-8 text-left" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
		    	<c:choose>
				<c:when test="${(not empty user.firstName) && (user.firstName ne 'null') && (user.firstName ne 'NULL') && (user.firstName ne 'Null')}">
		    		<%= user.getFirstName() %> 
		    	</c:when>
		    	</c:choose>
		    	<c:choose>
				<c:when test="${(not empty user.lastName) && (user.lastName ne 'null') && (user.lastName ne 'NULL') && (user.lastName ne 'Null')}">
					<%= user.getLastName() %>  
				</c:when>
				</c:choose>
		    </span>

	</div>
	
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	DateOfBirth :  
	    </span>
	    <span class="col-sm-8 text-left" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	<%= Util.parseDateToString(user.getDateOfBirth(), "yyyy-MM-dd") %> 
	    </span>
	</div>
	
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Postal Address :  
	    </span>
	    <span class="col-sm-8 text-left" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	  <c:choose>
				<c:when test="${(not empty user.addressStreet) && (user.addressStreet ne 'null') && (user.addressStreet ne 'NULL') && (user.addressStreet ne 'Null')}">
					<%= user.getAddressStreet() %> 
					<br>
				</c:when>
				</c:choose>
				<c:choose>
				<c:when test="${(not empty user.addressCity) && (user.addressCity ne 'null') && (user.addressCity ne 'NULL') && (user.addressCity ne 'Null')}">
					<%= user.getAddressCity() %> 
				</c:when>
				</c:choose>
				<c:choose>
				<c:when test="${(not empty user.addressState) && (user.addressState ne 'null') && (user.addressState ne 'NULL') && (user.addressState ne 'Null')}">
					&nbsp; <%= user.getAddressState() %> 
					<br>
				</c:when>
				</c:choose>
				<c:choose>
				<c:when test="${(not empty user.addressPostcode) && (user.addressPostcode ne 'null') && (user.addressPostcode ne 'NULL') && (user.addressPostcode ne 'Null')}">
					<%= user.getAddressPostcode() %> 
					<br>
				</c:when>
				</c:choose>
				<c:choose>
				<c:when test="${(not empty user.addressCountry) && (user.addressCountry ne 'null') && (user.addressCountry ne 'NULL') && (user.addressCountry ne 'Null')}">
					<%= user.getAddressCountry() %> 
				</c:when>
				</c:choose>
	    </span>
	</div>
	
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Credit Card :  
	    </span>
	    <c:choose>
		<c:when test="${(not empty user.creditCard) && (user.creditCard ne 'null') && (user.creditCard ne 'NULL') && (user.creditCard ne 'Null')}">
		    <span class="col-sm-8 text-left" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
		    	  <%= user.getCreditCard() %>
		    </span>
		 </c:when>
		 </c:choose>
	</div>
	
	<div class="form-group col-sm-12 text-right">
	    <a href="profile.jsp?state=edit" class="btn btn-warning btn-xs" role="button">Edit Profile</a>
	</div>
	
	<%
			}else{
			%>
			
			<!-- This part of profile is for EDIT PROFILE purpose -->
	
			
	<h1 style='font-weight: 100;letter-spacing: 2px'> Your Profile </h1>
	<hr>

	<c:if test="${param.state == 'failed'}">
		<h4 style="color: red;"><span class="glyphicon glyphicon-exclamation-sign"></span> Profile update failed. Your email address is already registered with Square Root.</h4>
	</c:if>
	<form class="form-horizontal" action='dispatcher?operation=updateProfile' method='post'>
	
	<div class="form-group col-sm-12">
	    <span class="col-sm-3 text-right" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Username :  
	    </span>
	    <span class="col-sm-3 text-left text-primary" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	<%= user.getUsername() %>  
	    </span>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='passwordLabel' for="password" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>
	    	Password* 
	    </label>
	    <div class="col-sm-6">
	      <input type="password" name='password' class="form-control input-sm" value="<%= user.getPassword() %>" 
	      style='border-radius:0;letter-spacing: 1px' id="password" 
	      placeholder="password (no space)" maxlength="64" pattern="[^\t\s\r\n]+" required/>
	    </div>
	</div>
		
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='emailLabel' for="email" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Email* </label>
	    <div class="col-sm-8">
	      <input type="email" name='email' class="form-control input-sm" value='<%= user.getEmail() %>' 
	      style='border-radius:0;letter-spacing: 1px' id="email" placeholder="valid email address" 
	      maxlength="255" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" required/>
	    </div>
	</div>
	
<c:choose>
<c:when test="${(empty user.nickname) || (user.nickname == 'null') || (user.nickname == 'NULL') || (user.nickname == 'Null')}">
	<div class="form-group col-sm-12">
		<label class="control-label col-sm-3" id='nicknameLabel' for="nickname" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Nickname </label>
		<div class="col-sm-8">
		    <input type="text" name='nickname' class="form-control input-sm" 
		      style='border-radius:0;letter-spacing: 1px' id="nickname" 
		      placeholder="tell us what people call you (alphanumeric and underscore)" maxlength="255" pattern="[^\W]+"/>
		</div>
	</div>
</c:when>
<c:otherwise>
	<div class="form-group col-sm-12">
		<label class="control-label col-sm-3" id='nicknameLabel' for="nickname" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Nickname </label>
		   <div class="col-sm-8">
		     <input type="text" name='nickname' class="form-control input-sm" value='<%= user.getNickname() %>' 
		      style='border-radius:0;letter-spacing: 1px' id="nickname" 
		      placeholder="tell us what people call you (alphanumeric and underscore)" maxlength="255" pattern="[^\W]+"/>
		   </div>
	</div>
</c:otherwise>
</c:choose>


<c:choose>
<c:when test="${(empty user.firstName) || (user.firstName == 'null') || (user.firstName == 'NULL') || (user.firstName == 'Null')}">	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='firstNameLabel' for="firstName" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Name </label>
	    <div class="col-sm-6">
	      <input type="text" name='firstName' class="form-control input-sm" 
	      style='border-radius:0;letter-spacing: 1px' id="firstName" placeholder="first name" maxlength="255" />
	    </div>
	</div>
</c:when>
<c:otherwise>
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='firstNameLabel' for="firstName" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Name </label>
	    <div class="col-sm-6">
	      <input type="text" name='firstName' class="form-control input-sm" value='<%= user.getFirstName() %>'
	      style='border-radius:0;letter-spacing: 1px' id="firstName" placeholder="first name" maxlength="255" />
	    </div>
	</div>
</c:otherwise>
</c:choose>
	
<c:choose>
<c:when test="${(empty user.lastName) || (user.lastName == 'null') || (user.lastName == 'NULL') || (user.lastName == 'Null')}">	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='lastNameLabel' for="lastName" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'> </label>
	    <div class="col-sm-6">
	      <input type="text" name='lastName' class="form-control input-sm" 
	      style='border-radius:0;letter-spacing: 1px' id="lastName" placeholder="last name" maxlength="255" />
	    </div>
	</div>
</c:when>
<c:otherwise>
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='lastNameLabel' for="lastName" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'> </label>
	    <div class="col-sm-6">
	      <input type="text" name='lastName' class="form-control input-sm" value='<%= user.getLastName() %>'
	      style='border-radius:0;letter-spacing: 1px' id="lastName" placeholder="last name" maxlength="255" />
	    </div>
	</div>
</c:otherwise>
</c:choose>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='dateOfBirth' for="dateOfBirth" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Date of Birth </label>
	    <div class="col-sm-6">
	      <input type="date" name='dateOfBirth' class="form-control input-sm" value='<%=  Util.parseDateToString(user.getDateOfBirth(), "yyyy-MM-dd") %>' 
	      style='border-radius:0;letter-spacing: 1px' id="dob" placeholder="date of birth (yyyy-mm-dd)" />
	    </div>
	</div>
	
<c:choose>
<c:when test="${(empty user.addressStreet) || (user.addressStreet == 'null') || (user.addressStreet == 'NULL') || (user.addressStreet == 'Null')}">
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" for="addressStreet" id='addressStreetLabel' style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Postal Address </label>
	    <div class="col-sm-8">
	      <input type="text" name='addressStreet' class="form-control input-sm" 
	      style='border-radius:0;letter-spacing: 1px' id="addressStreet" placeholder=" street " maxlength="255" >
	    </div>	    
	</div>
</c:when>
<c:otherwise>
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" for="addressStreet" id='addressStreetLabel' style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Postal Address </label>
	    <div class="col-sm-8">
	      <input type="text" name='addressStreet' class="form-control input-sm" value='<%= user.getAddressStreet() %>' 
	      style='border-radius:0;letter-spacing: 1px' id="addressStreet" placeholder=" street " maxlength="255" >
	    </div>	    
	</div>
</c:otherwise>
</c:choose>
	

	<div class='col-sm-12'>
	  	<div class='col-sm-3'></div>
	  	<c:choose>
		<c:when test="${(empty user.addressCity) || (user.addressCity == 'null') || (user.addressCity == 'NULL') || (user.addressCity == 'Null')}">
		  	<div class='col-sm-4' style='margin-left:0;padding-left:0'>
		  		<input type="text" name='addressCity' class="form-control input-sm" 
		  		style='border-radius:0;letter-spacing: 1px' id="addressCity" placeholder="city" maxlength="255" >
		  	</div>
	  	</c:when>
	  	<c:otherwise>
	  		<div class='col-sm-4' style='margin-left:0;padding-left:0'>
		  		<input type="text" name='addressCity' class="form-control input-sm" value='<%=user.getAddressCity() %>' 
		  		style='border-radius:0;letter-spacing: 1px' id="addressCity" placeholder="city" maxlength="255" >
		  	</div>
	  	</c:otherwise>
		</c:choose>
		
		<c:choose>
		<c:when test="${(empty user.addressState) || (user.addressState == 'null') || (user.addressState == 'NULL') || (user.addressState == 'Null')}">
		  	<div class='col-sm-4'>
		  		<input type="text" name='addressState' class="form-control input-sm" 
		  		style='border-radius:0;letter-spacing: 1px;width:95%' id="addressState" placeholder="state" maxlength="255" >
		  	</div>
	  	</c:when>
	  	<c:otherwise>
	  		<div class='col-sm-4'>
		  		<input type="text" name='addressState' class="form-control input-sm" value='<%=user.getAddressState() %>' 
		  		style='border-radius:0;letter-spacing: 1px;width:95%' id="addressState" placeholder="state" maxlength="255" >
		  	</div>
	  	</c:otherwise>
	  	</c:choose>
	</div>

	
	<div class='col-sm-12' style='margin-top:10px;margin-bottom:15px'>
	  	<div class='col-sm-3'></div>
	  	<c:choose>
		<c:when test="${(empty user.addressCountry) || (user.addressCountry == 'null') || (user.addressCountry == 'NULL') || (user.addressCountry == 'Null')}">
		  	<div class='col-sm-4' style='margin-left:0;padding-left:0'>
		  		<select class="form-control input-sm" name='addressCountry'> 
		  			<option value="" selected="selected">Country</option>
		  			<jsp:include page="/country.jsp"/>
		  		</select>
		  	</div>
	  	</c:when>
	  	<c:otherwise>
	  		<div class='col-sm-4' style='margin-left:0;padding-left:0'>
		  		<select class="form-control input-sm" name='addressCountry'> 
		  			<option value="${user.addressCountry}" selected="selected">${user.addressCountry}</option>
		  			<jsp:include page="/country.jsp"/>
		  		</select>
		  	</div>
	  	</c:otherwise>
	  	</c:choose>
	  	
	  	<c:choose>
		<c:when test="${(empty user.addressPostcode) || (user.addressPostcode == 'null') || (user.addressPostcode == 'NULL') || (user.addressPostcode == 'Null')}">
		  	<div class='col-sm-4'>
		  		<input type="text" name='addressPostcode' class="form-control input-sm" 
		  		style='border-radius:0;letter-spacing: 1px;width:95%' id="addressPostcode" placeholder="postcode" maxlength="20" >
		  	</div>
		</c:when>
		<c:otherwise>
			<div class='col-sm-4'>
		  		<input type="text" name='addressPostcode' class="form-control input-sm" value='<%= user.getAddressPostcode() %>' 
		  		style='border-radius:0;letter-spacing: 1px;width:95%' id="addressPostcode" placeholder="postcode" maxlength="20" >
		  	</div>
		</c:otherwise>
		</c:choose>
	</div>
	
<c:choose>
<c:when test="${(empty user.creditCard) || (user.creditCard == 'null') || (user.creditCard == 'NULL') || (user.creditCard == 'Null')}">
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='creditCardLabel' for="creditCard" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Credit Card </label>
	    <div class="col-sm-6">
	      <input type="text" name='creditCard' class="form-control input-sm" 
	      style='border-radius:0;letter-spacing: 1px' id="creditCard" placeholder="16 digit card number" maxlength="16" pattern="\d{16}"/>
	    </div>
	</div>
</c:when>
<c:otherwise>
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='creditCardLabel' for="creditCard" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Credit Card </label>
	    <div class="col-sm-6">
	      <input type="text" name='creditCard' class="form-control input-sm" value='<%= user.getCreditCard() %>' 
	      style='border-radius:0;letter-spacing: 1px' id="creditCard" placeholder="16 digit card number" maxlength="16" pattern="\d{16}"/>
	    </div>
	</div>
</c:otherwise>
</c:choose>
	
	<div class="form-group col-sm-12 text-right">
		<button type="submit" value='register' class="btn btn-success btn-sm" style='border-radius:0;letter-spacing: 1px'><span class='text-primary'>Update</span></button>
	</div>
	</form>
	
			
			<%
			}
		}
	%>
	
	</div>
	
	<div class='col-sm-2'></div>
	
	</div>
	
	<jsp:include page="/footer.jsp"/>
</body>
</html>