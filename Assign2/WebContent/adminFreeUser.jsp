<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">

<link rel="stylesheet" type="text/css" href="css/dataTables.css">
<script src="js/tablesorter.js"></script>
<script>
$(document).ready(function() {
    $('#mytable').dataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>

<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>Free User Page</title>

</head>
<body>

<jsp:include page="/header.jsp"/>

	
			
			
<div class="container">
	<div class="row">
		
        
        <div class="col-md-12">
        	<div class="page-header">
			<h2>Banned User List</h2>
			</div>
		<c:choose>
			<c:when test="${(not empty user) && user.isAdmin}"> 
			<c:choose>
				<c:when test="${not empty param.username}">
					<h4 style="color: #4ab025;"><span class="glyphicon glyphicon-ok-sign"></span> Successfully freed user with username: ${param.username}.</h4>
				</c:when>
			</c:choose>
			<c:choose>
				<c:when test="${empty userInfo}">
				 	<h4>No user to free.</h4>
				</c:when>
			</c:choose>		
        
              <table id="mytable" class="table ">
                   
                   <thead>
                   
                   <th>Username</th>
                   <th>User Detail</th>
                   <th>User Status</th>
                   <th>Actions</th>

                   </thead>
    <tbody>
    <c:forEach var="user" items="${userInfo}">
    
    <tr><td><b>${user.username}</b></td>
    
	<td>

		<table class="table">
			<thead>
				<td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
				<td></td>
			</thead>
			<tbody>
				<tr>
					<td >Nickname</td>
					<td >${user.nickname}</td>
				</tr>
				<tr>
					<td >Full Name</td>
					<td >${user.firstName}&nbsp;&nbsp;${user.lastName}</td>
				</tr>
				<tr>
					<c:set var="dob" value="${fn:substringBefore(user.dateOfBirth, ' ')}" />

					<td >Date of Birth</td>
					<td >${dob}</td>
				</tr>
				<tr>
					<td >Postage Address</td>
					<td >${user.addressStreet}<br>
						${user.addressCity}&nbsp;&nbsp;${user.addressState}<br>
						${user.addressCountry}&nbsp;&nbsp;${user.addressPostcode}</td>
				</tr>
			</tbody>
		</table>

    </td>
	<td>User Status: ${user.userStatus}</td>
	<td>
		<br>
	 	<!-- <iframe name="hiddenFrame" class="hide"></iframe> -->
	 	<c:set var="myform" value="${user.username}" />
		<form action="dispatcher?operation=freeuser" method="POST" id="${myform}" name="${myform}" >
			<input type="hidden" name="username" value="${user.username}" />
			<input type="submit" class="btn btn-default" value="Free User"  />
		</form>
	</td>
</c:forEach>

    
   
    
   
    
    </tbody>
        
</table>
			</c:when>
			<c:otherwise>
				<h4>Please login as a admin</h4>
			</c:otherwise>
		</c:choose>
</div>
</div>
</div>

            

<jsp:include page="/footer.jsp"/>

</body>
</html>