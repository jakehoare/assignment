<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>


<title>Advanced Search</title>

</head>
<body>

<jsp:include page="/header.jsp"/>

		
	<div class='container-fluid'>
	<br><br><BR>
	
	<div class='col-sm-2'></div>
	
	<div class='col-sm-8 text-center center-block' style='border: 1px #e4e4e4 solid;'>
	<h1 style='font-weight: 100;letter-spacing: 2px'> Advanced Search </h1>
	<hr>
	
	<form class="form-horizontal" action="dispatcher?operation=search" method='post'>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='titleLabel' for="title" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Keywords </label>
	    <div class="col-sm-5">
	    	<input type=text name=keywords class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' maxlength="100" autofocus>					
	    </div>
	    <div class="col-sm-3">
	    	<select name="match" class="form-control input-sm" style='border-radius:0;letter-spacing: 1px'>
				<option value="All">All words</option>
				<option value="Any">Any words</option>
			</select>
	    </div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='categoryLabel' for="category" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Category </label>
	    <div class="col-sm-3">
	      <select name='category' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="category">
	      		<option value="All">All</option>
				<option value="Arts">Arts</option>
				<option value="Books">Books</option>
				<option value="Clothing">Clothing</option>
				<option value="Computers">Computers</option>
				<option value="Electronics">Electronics</option>
				<option value="Games">Games</option>
				<option value="Shoes">Shoes</option>
				<option value="Sports">Sports</option>
		</select>		
	    </div>
	</div>
	
	<div class="form-group col-sm-12" >
	    <label class="control-label col-sm-3" for="addressStreet" id='addressStreetLabel' style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Postal Address </label>
	  	<div class="col-sm-8">
	    </div>	 
	</div>
	
	<div class='col-sm-12'>
	  	<div class='col-sm-3'></div>
	  	<div class='col-sm-4' style='margin-left:0;padding-left:0'>
	  		<input type="text" name='city' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="addressCity" placeholder="city" maxlength="50">
	  	</div>
	  	<div class='col-sm-4'>
	  		<input type="text" name='state' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px;width:95%' id="addressState" placeholder="state" maxlength="50">
	  	</div>
	</div>
	
	<div class='col-sm-12' style='margin-top:10px;margin-bottom:15px'>
	  	<div class='col-sm-3'></div>
	  	<div class='col-sm-4' style='margin-left:0;padding-left:0'>
	  		<select class="form-control input-sm"  name='country'> 
	  			<option value="" selected="selected">All Countries</option>
	  			<jsp:include page="/country.jsp"/>
	  		</select>
	  	</div>
	  	<div class='col-sm-4'>
	  		<input type="text" name='postCode' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px;width:95%' id="addressPostcode" placeholder="postcode" maxlength="20" pattern="[A-Za-z\d]+">
	  	</div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='currencyLabel' for="currency" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Currency </label>
	    <div class="col-sm-2">
	      <select name='currency' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="currency">
	      	<option value="AUD">AUD</option>
		  	<option value="EUR">EUR</option>
		  	<option value="GBP">GBP</option>
		  	<option value="USD">USD</option>
		</select>		
	    </div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='startingBidLabel' for="startingBid" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Starting Bid </label>
	    <div class="col-sm-4">
	      <input type="number" name='priceMin' min="0" step="any" class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' placeholder="from this price $"/>
	    </div>
	    <div class="col-sm-4">
	      <input type="number" name='priceMax' min="0" step="any" class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' placeholder="to this price $"/>
	    </div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='endMinutesLabel' for="endMinutes" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>End Time </label>
	    <div class="col-sm-4" >
	      <select name="timeOption" class="form-control input-sm" style='border-radius:0;letter-spacing: 1px'>
			<option value="Within">Ending within</option>
			<option value="MoreThan">Ending in more than</option>
		  </select>
	    </div>
	    <div class="col-sm-4">
	      <select name="timeInMinutes" class="form-control input-sm" style='border-radius:0;letter-spacing: 1px'>
			<option value="Select">Select</option>
			<option value="5">5 minutes</option>
			<option value="10">10 minutes</option>
			<option value="15">15 minutes</option>
			<option value="30">30 minutes</option>
			<option value="45">45 minutes</option>
			<option value="60">60 minutes</option>
		  </select>
	    </div>
	</div>

	<div class='col-sm-12 text-center' style='margin-bottom:10px;margin-top:10px'>
    	<button type="submit"  value='Search' class="btn btn-default btn-sm" style='border-radius:0;letter-spacing: 1px'><span class='text-primary'>Search</span></button>    	    	
      </div>
	</form>
		
	<div class='col-sm-12 text-left' style='margin-bottom:10px;margin-top:10px'>
		
	</div>	
	</div>
	
	
	
	<div class='col-sm-2'></div>
	
	</div>
	
<jsp:include page="/footer.jsp"/>

</body>
</html>
