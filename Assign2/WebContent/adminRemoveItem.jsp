<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">

<link rel="stylesheet" type="text/css" href="css/dataTables.css">
<script src="js/tablesorter.js"></script>
<script>
$(document).ready(function() {
    $('#mytable').dataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>

<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>Remove Item Page</title>

</head>
<body>

<jsp:include page="/header.jsp"/>

	
			
			
<div class="container">
	<div class="row">
		
    <div class="col-md-12">
       	<div class="page-header">
					<h2>Not Live Auctions</h2>
				</div>
		<c:choose>
			<c:when test="${(not empty user) && user.isAdmin}">
				<c:choose>
					<c:when test="${not empty param.remove}">
						<h4 style="color: #4ab025;"><span class="glyphicon glyphicon-ok-sign"></span> Successfully removed auction with item ID: ${param.remove}.</h4>
					</c:when>
				</c:choose>
				<c:choose>
					<c:when test="${empty itemInfo}">
					 	<h4>No auction to remove.</h4>
					</c:when>
				</c:choose>		


			<table id="mytable" class="table ">
                   
                   <thead>
                   
                   <th></th>
                   <th>Title</th>
                   <th>Seller</th>
                   <th>Price</th>
                   <th>EndTime</th>
                   <th>Actions</th>

                   </thead>
    <tbody>
    <c:forEach var="item" items="${itemInfo}">
    
    <tr><td><img src="${item.picture}" alt="${item.title}" width="220" height="280"></td>
	<td>${item.title}</td>
	<td>${item.seller.username}</td>
		<c:set var="string1" value="${item.currency}" />
		<c:choose>
			<c:when test="${string1 == 'AUD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'USD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'GBP'}">
				<c:set var="cur" value="£" />
			</c:when>
			<c:otherwise>
				<c:set var="cur" value="€" />
			</c:otherwise>
		</c:choose>
	<td>${string1} <br>
		Best Bid: ${cur}${item.bestBid}<br>
		Bidding Increments: ${cur}${item.bidIncrements}
	</td>
	<td>End Time: ${item.endTime}<br>
		Status: ${item.itemStatus}</td>
	<td>
		<form action="dispatcher?operation=itemInfo" method="POST" id="${item.title}" name="${item.title}" >
			<input type="hidden" name="itemID" value="${item.itemId}" />
			<input type="submit" class="btn btn-default" value="Show Info"  />
		</form>

		<br>
	 	<!-- <iframe name="hiddenFrame" class="hide"></iframe> -->
	 	<c:set var="myform" value="${item.itemId}" />
		<form action="dispatcher?operation=removeitem" method="POST" id="${myform}" name="${myform}" >
			<input type="hidden" name="itemId" value="${item.itemId}" />
			<input type="submit" class="btn btn-default" value="Remove Auction"  />
		</form>
	</td>
</c:forEach>

    
   
    
   
    
    </tbody>
        
</table>
			</c:when>
			<c:otherwise>
				<h4>Please login as a admin</h4>
			</c:otherwise>
		</c:choose>
                
              

</div>
</div>
</div>

            

<jsp:include page="/footer.jsp"/>

</body>
</html>