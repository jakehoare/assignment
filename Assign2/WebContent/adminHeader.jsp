<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" >
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a><img style="margin:10px auto;display:block" src="images/log2.png" alt="Square Root - Online Auction" width="180" height="30"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
      	<li><a > Hi <b>${user.username}</b>!</a></li>
      	 <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> My Profile<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="forward3.jsp"><span class="glyphicon glyphicon-star"></span> My Collections</a></li>
            <li><a href="dispatcher?operation=listmessages"><span class="glyphicon glyphicon-envelope"></span> My Messages</a></li>
            <li><a href="profile.jsp"><span class="glyphicon glyphicon-edit"></span> Account Settings</a></li>
            <li><a href="forward2.jsp"><span class="glyphicon glyphicon-heart"></span> Wish List</a></li>
         	<li><a href="additem.jsp"><span class="glyphicon glyphicon-plus"></span> Add Item</a></li>
            <li><a href="logout.jsp"><span class="glyphicon glyphicon-off"></span> Sign Out</a></li>
          </ul>
        </li>
        <li><a href="#menu-toggle" class="btn btn-default" style="color:#fff;background:#000;border:0;text-decoration:none;" id="menu-toggle"><span class="glyphicon glyphicon-th-list"></span> Menu</a></li>
      </ul>
    </div>
    
  </div>
</nav>






<div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">    
  					<a></a>
                </li>
                <li>
                    <a href="forward.jsp"><span class="glyphicon glyphicon-home"></span> Home</a>
                </li>
                 <li>
                    <a href="forward6.jsp"><span class="glyphicon glyphicon-plus"></span> Auction to Live</a>
                </li>
                <li>
                    <a href="forward4.jsp"><span class="glyphicon glyphicon-minus"></span> Halt Auction</a>
                </li>
                <li>
                    <a href="forward8.jsp"><span class="glyphicon glyphicon-ok-circle"></span> Free User</a>
                </li>
                <li>
                    <a href="forward5.jsp"><span class="glyphicon glyphicon-lock"></span> Ban User</a>
                </li>
                <li>
                    <a href="forward7.jsp"><span class="glyphicon glyphicon-trash"></span> Remove Auction</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
        	<br>
			<h1><a href="forward.jsp"><img style="margin:0px auto;display:block" src="images/logo4.png" alt="Square Root - Online Auction"></a></h1>
        	<nav class="navbar navbar-default" >
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar1">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>                        
			      </button>
			      <a class="navbar-brand" href="forward.jsp">Admin</a>
			    </div>
			    <div class="collapse navbar-collapse" id="myNavbar1">
			   		<div class="pull-left">
			        	<form class="navbar-form" role="search" action="dispatcher?operation=search" method="POST">
			        		<div class="input-group">
			        			<select class="form-control" style="width: 100px;" name="category">
										  <option value="All">All</option>
										  <option value="Arts">Arts</option>
										  <option value="Books">Books</option>
										  <option value="Clothing">Clothing</option>
										  <option value="Computers">Computers</option>
										  <option value="Electronics">Electronics</option>
										  <option value="Games">Games</option>
										  <option value="Shoes">Shoes</option>
										  <option value="Sports">Sports</option>
										</select>
			            		<input type="text" class="form-control" placeholder="Search..." name="keywords" id="srch-term" style="width: 330px;" >
			            		<div class="input-group-btn">
			                		<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			            		</div>
			        		</div>
			        	</form>
			       </div>
			       
				   <ul class="nav navbar-nav">
				   		<li><a href="advancedSearch.jsp">Advanced Search</a></li>
				   </ul>
			    </div>
			  </div>
			</nav>
</body>
</html>
