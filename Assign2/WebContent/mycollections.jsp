<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">

<link rel="stylesheet" type="text/css" href="css/dataTables.css">
<script src="js/tablesorter.js"></script>
<script>
$(document).ready(function() {
    $('#mytable').dataTable( {
        "pagingType": "full_numbers"
    } );
} );
</script>

<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>My Collections Page</title>
</head>
<body>
<jsp:include page="/header.jsp"/>

<br>


<div class="container">
<div class="page-header">
<h2>My Collections</h2>
</div>

<c:choose>
	<c:when test="${empty myCollections}">
	 	<h4>Your collection is empty.</h4>
	</c:when>
</c:choose>

<table id="mytable" class="table">
<thead>
<tr><th></th>
	<th>Title</th>
	<th>Price</th>
	<th>EndTime</th>
	<th>Actions</th>
</tr>
</thead>
<tbody>
<c:forEach var="item" items="${myCollections}">
<tr><td><img src="${item.picture}" alt="${item.title}" width="220" height="280"></td>
	<td>${item.title}</td>
		<c:set var="string1" value="${item.currency}" />
		<c:choose>
			<c:when test="${string1 == 'AUD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'USD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'GBP'}">
				<c:set var="cur" value="£" />
			</c:when>
			<c:otherwise>
				<c:set var="cur" value="€" />
			</c:otherwise>
		</c:choose>
	<td>${string1} <br>
		ReservePrice: ${cur}${item.reservePrice}<br>
		Best Bid: ${cur}${item.bestBid}<br>
		Bidding Increments: ${cur}${item.bidIncrements}
	</td>
	<td>End Time: <fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${item.endTime}" /><br>
		Status: ${item.itemStatus}
	</td>
	<td>
		<form action="dispatcher?operation=itemInfo" method="POST" id="${item.title}" name="${item.title}" >
			<input type="hidden" name="itemID" value="${item.itemId}" />
			<input type="submit" class="btn btn-default" value="Show Info"  />
		</form>
		<br>
		<c:choose>
			<c:when test="${item.itemStatus == 'PENDING'}">
	 			<form action="dispatcher?operation=pending" method="POST"  >
					<input type="hidden" name="itemID" value="${item.itemId}" />
					<input type="hidden" name="action" value="ACCEPT" />					
					<input type="submit" class="btn btn-default" value="Accept Bid"  />
				</form>
				<br>
				<form action="dispatcher?operation=pending" method="POST" >
					<input type="hidden" name="itemID" value="${item.itemId}" />
					<input type="hidden" name="action" value="REJECT" />					
					<input type="submit" class="btn btn-default" value="Reject Bid"  />
				</form>
	 		</c:when>
			<c:otherwise>
				<!-- User cannot take any action -->
			</c:otherwise>
	 	</c:choose>
	</td>
</tr>
</c:forEach>
</tbody>
</table>

</div>



<jsp:include page="/footer.jsp"/>

</body>
</html>
