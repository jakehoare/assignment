<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>


<title>Added</title>

</head>
<body>


<jsp:include page="/header.jsp"/>

<div class='container'>
   <div class="col-md-8">
	<div class="panel3 panel-default">
		<div class="panel-heading">
			<h3 style="color: #4ab025;"><span class="glyphicon glyphicon-ok-sign"></span> Congratulations!</h3>
		</div>
		<div class="panel-body">
			<h4 style="color: #4ab025;">
				Your new auction item has been added.
				<br>
			</h4>
			<br>
			<form action="additem.jsp" method="POST">
				<button class="btn btn-success btn-lg" type="submit"><span class="glyphicon glyphicon-plus"></span> <b>Add Another</b></button>
			</form>
			<br>
		</div>
	</div>
  </div>
</div>

<jsp:include page="/footer.jsp"/>

</body>
</html>
