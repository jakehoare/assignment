<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>Register Success Page</title>

</head>
<body>

<jsp:include page="/header.jsp"/>

<div class='container'>
   <div class="col-md-8">
	<div class="panel3 panel-default">
		<div class="panel-heading">
			<h3 style="color: #4ab025;"><span class="glyphicon glyphicon-ok-sign"></span> Congratulations!</h3>
		</div>
		<div class="panel-body">
			<h4 style="color: #4ab025;">
				Welcome to Square Root Online Auction.<br><br><br>
				A confirmation letter will be sent to <%= request.getParameter("emailAdd") %> in a minute.
				<br><br>
				To complete your registration, please follow the link given 
				in the confirmation letter to confirm your email address.<br><br><br>
				Thank you.
				<br>
			</h4>
		</div>
	</div>
  </div>
</div>
	
<jsp:include page="/footer.jsp"/>
</body>
</html>