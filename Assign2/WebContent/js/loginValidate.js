/**
 * Validations for login
 */

var usernameCheck,
    passwordCheck;

function validate(){
	
	var username = document.getElementById('username').value;
	if(username == ''){
		usernameCheck = false;
		document.getElementById('usernameLabel').style.color = 'red';
	}else{
		document.getElementById('usernameLabel').style.color = 'black';
		usernameCheck = true;
	}
	
	
	var password = document.getElementById('password').value;
	if(password == ''){
		passwordCheck = false;
		document.getElementById('passwordLabel').style.color = 'red';
	}else{
		document.getElementById('passwordLabel').style.color = 'black';
		passwordCheck = true;
	}
	if(usernameCheck && passwordCheck)
		return true;
	else
		return false;
}
