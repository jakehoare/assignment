/**
 * Validations for additem.jsp
 */

var titleCheck,
	descriptionCheck,
	pictureCheck,
	reservePriceCheck,
	bidIncrementsCheck,
	startingBidCheck,
	endMinutesCheck;
	

function validate(){
	
	
	var title = document.getElementById('title').value;
	if(title == ''){
		titleCheck = false;
		document.getElementById('titleLabel').style.color = 'red';
	}else{
		document.getElementById('titeLabel').style.color = 'black';
		titleCheck = true;
	}
	
	var description = document.getElementById('description').value;
	if(description == ''){
		descriptionCheck = false;
		document.getElementById('descriptionLabel').style.color = 'red';
	}else{
		document.getElementById('descriptionLabel').style.color = 'black';
		descriptionCheck = true;
	}
	
	var picture = document.getElementById('picture').value;
	if(picture == ''){
		pictureCheck = false;
		document.getElementById('pictureLabel').style.color = 'red';
	}else{
		document.getElementById('pictureLabel').style.color = 'black';
		pictureCheck = true;
	}
	
	var reservePrice = document.getElementById('reservePrice').value;
	if(isNaN(parseFloat(reservePrice)) || parseFloat(reservePrice) < 0 || parseFloat(reservePrice) > 999999){
		reservePriceCheck = false;
		document.getElementById('reservePriceLabel').style.color = 'red';
	}else{
		document.getElementById('reservePriceLabel').style.color = 'black';
		reservePriceCheck = true;
	}
	
	var bidIncrements = document.getElementById('bidIncrements').value;
	if(isNaN(parseFloat(bidIncrements)) || parseFloat(bidIncrements) < 0.01 || parseFloat(bidIncrements) > 999999){
		bidIncrementsCheck = false;
		document.getElementById('bidIncrementsLabel').style.color = 'red';
	}else{
		document.getElementById('bidIncrementsLabel').style.color = 'black';
		bidIncrementsCheck = true;
	}
	
	var startingBid = document.getElementById('startingBid').value;
	if(isNaN(parseFloat(startingBid))){
	//if(isNaN(parseFloat(startingBid)) || parseFloat(startingBid) < 0 || parseFloat(startingBid) > 999999){
		startingBidCheck = false;
		document.getElementById('startingBidLabel').style.color = 'red';
	}else{
		document.getElementById('startingBidLabel').style.color = 'black';
		startingBidCheck = true;
	}
	
	var endMinutes = document.getElementById('endMinutes').value;
	if(isNaN(parseInt(endMinutes)) || parseInt(endMinutes) < 3 || parseInt(endMinutes) > 60){
		endMinutesCheck = false;
		document.getElementById('endMinutesLabel').style.color = 'red';
	}else{
		document.getElementById('endMinutesLabel').style.color = 'black';
		endMinutesCheck = true;
	}
		
	if(titleCheck && descriptionCheck && pictureCheck && reservePriceCheck && bidIncrementsCheck && startingBidCheck && endMinutesCheck)
		return true;
	else
		return false;
	
}


