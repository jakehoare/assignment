/**
 * Validations for Register.jsp
 */

var usernameCheck,
	passwordCheck,
	emailCheck,
	nicknameCheck,
	firstNameCheck,
	lastNameCheck,
	addressCheck,
	creditCardCheck;


function validate(){
	
	
	var username = document.getElementById('username').value;
	if(username == ''){
		usernameCheck = false;
		document.getElementById('usernameLabel').style.color = 'red';
	}else{
		document.getElementById('usernameLabel').style.color = 'black';
		usernameCheck = true;
	}
	
	var password = document.getElementById('password').value;
	if(password == ''){
		passwordCheck = false;
		document.getElementById('passwordLabel').style.color = 'red';
	}else{
		document.getElementById('passwordLabel').style.color = 'black';
		passwordCheck = true;
	}
	
	var email = document.getElementById('email').value;
	if(email == ''){
		emailCheck = false;
		document.getElementById('emailLabel').style.color = 'red';
	}else{
		document.getElementById('emailLabel').style.color = 'black';
		emailCheck = true;
	}
/*	
	var nickname = document.getElementById('nickname').value;
	if(nickname == ''){
		nicknameCheck = false;
		document.getElementById('nicknameLabel').style.color = 'red';
	}else{
		document.getElementById('nicknameLabel').style.color = 'black';
		nicknameCheck = true;
	}
	
	var firstName = document.getElementById('firstName').value;
	if(firstName == ''){
		firstNameCheck = false;
		document.getElementById('firstNameLabel').style.color = 'red';
	}else{
		document.getElementById('firstNameLabel').style.color = 'black';
		firstNameCheck = true;
	}
	
	var lastName = document.getElementById('lastName').value;
	if(lastName == ''){
		lastNameCheck = false;
		document.getElementById('lastNameLabel').style.color = 'red';
	}else{
		document.getElementById('lastNameLabel').style.color = 'black';
		lastNameCheck = true;
	}
	
	var addressStreet = document.getElementById('addressStreet').value;
	var addressCity = document.getElementById('addressCity').value;
	var addressState = document.getElementById('addressState').value;
	var addressCountry = document.getElementById('addressCountry').value;
	var addressPostcode = document.getElementById('addressPostcode').value;
	
	if(addressStreet == '' || addressCity == ''|| addressState == ''|| addressCountry == ''|| isNaN(parseInt(addressPostcode))){
		addressCheck = false;
		document.getElementById('addressStreetLabel').style.color = 'red';
	}else{
		document.getElementById('addressStreetLabel').style.color = 'black';
		addressCheck = true;
	}
	
	
	var creditCard = document.getElementById('creditCard').value;
	if(isNaN(parseInt(creditCard))){
		creditCardCheck = false;
		document.getElementById('creditCardLabel').style.color = 'red';
	}else{
		document.getElementById('creditCardLabel').style.color = 'black';
		creditCardCheck = true;
	}
	*/
	if(usernameCheck && passwordCheck && emailCheck)
		return true;
	else
		return false;
	
}
