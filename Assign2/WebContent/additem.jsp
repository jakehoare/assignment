<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">
<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>
<script src="js/newItemValidate.js"></script>

<title>New Item Page</title>


</head>
<body>

<jsp:include page="/header.jsp"/>

		
	<div class='container-fluid'>
	<br><br><BR>
	
	<div class='col-sm-2'></div>
	
	<div class='col-sm-8 text-center center-block' style='border: 1px #e4e4e4 solid;'>
	<h1 style='font-weight: 100;letter-spacing: 2px'> Add a New Item for Auction </h1>
	<hr>
	
	<form class="form-horizontal" action='dispatcher?operation=newitem' method='post'>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='titleLabel' for="title" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Title* </label>
	    <div class="col-sm-5">
	      <input type="text" name='title' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="title" placeholder="title" required autofocus/>
	    </div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='categoryLabel' for="category" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Category* </label>
	    <div class="col-sm-3">
	      <select name='category' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="category">
	      	<option value="ARTS">Arts</option>
		  	<option value="BOOKS">Books</option>
		  	<option value="CLOTHING">Clothing</option>
		  	<option value="COMPUTERS">Computers</option>
		  	<option value="ELECTRONICS">Electronics</option>
		  	<option value="GAMES">Games</option>
		  	<option value="SHOES">Shoes</option>
		  	<option value="SPORTS">Sports</option>
		</select>		
	    </div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='descriptionLabel' for="description" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Description </label>
	    <div class="col-sm-8">
	      <input type="text" name='description' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="email" placeholder="description"/>
	    </div>
	</div>
		
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='pictureLabel' for="picture" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Picture </label>
	    <div class="col-sm-8">
	      <input type="text" name='picture' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="picture" placeholder="picture"/>
	    </div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='currencyLabel' for="currency" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Currency* </label>
	    <div class="col-sm-2">
	      <select name='currency' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="currency">
	      	<option value="AUD">AUD</option>
		  	<option value="EUR">EUR</option>
		  	<option value="GBP">GBP</option>
		  	<option value="USD">USD</option>
		</select>		
	    </div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='reservePriceLabel' for="reservePrice" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Reserve Price* </label>
	    <div class="col-sm-5">
	      <input type="number" name='reservePrice' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="reservePrice" placeholder="minimum sale price" min="0" step="0.01" max="999999" required/>
	    </div>
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" for="bidIncrements" id='bidIncrementsLabel' style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Bid Increments* </label>
	    <div class="col-sm-5">
	      <input type="number" name='bidIncrements' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="bidIncrements" placeholder="minimum bid increases" min="0.01" step="0.01" max="999999" required/>
	    </div>	    
	</div>
	
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='startingBidLabel' for="startingBid" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Starting Bid* </label>
	    <div class="col-sm-5">
	      <input type="number" name='startingBid' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="startingBid" placeholder="where to start the auction" min="0" step="0.01" max="999999" required/>
	    </div>
	</div>
	
	<div class="form-group col-sm-12">
	    <label class="control-label col-sm-3" id='endMinutesLabel' for="endMinutes" style='font-weight: 200;font-size: 120%;letter-spacing: 1px;'>Auction Period* </label>
	    <div class="col-sm-5">
	      <input type="number" name='endMinutes' class="form-control input-sm" style='border-radius:0;letter-spacing: 1px' id="endMinutes" placeholder="between 3 and 60 minutes" min="3" step="1" max="60" required/>
	    </div>
	</div>

	<div class='col-sm-12 text-center' style='margin-bottom:10px;margin-top:10px'>
    	<button type="submit" onclick='return validate()' value='newitem' class="btn btn-default btn-sm" style='border-radius:0;letter-spacing: 1px'><span class='text-primary'>Submit</span></button>    	    	
      </div>
	
	</form>
	
	<div class='col-sm-12 text-left' style='margin-bottom:10px;margin-top:10px'>
		Fields marked with * are required.
	</div>
	
	<div class='col-sm-12 text-left' style='margin-bottom:10px;margin-top:10px'>
		
	</div>	
	
	</div>
	
	<div class='col-sm-2'></div>
	
	</div>
	
<jsp:include page="/footer.jsp"/>
</body>
</html>




