<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="edu.unsw.comp9321.*
							, java.util.*
							, org.xml.sax.InputSource
							, org.w3c.dom.*
							, javax.xml.parsers.*
							, javax.xml.xpath.*"
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
<link rel="stylesheet" type="text/css" href="css/admin2.css">

<link rel="stylesheet" type="text/css" href="css/dataTables.css">
<script src="js/tablesorter.js"></script>


<%@ include file="css/css.html" %>
<%@ include file="css/mystyle.html" %>

<title>Search Result</title>

<script>
  $(function(){
	    $('.addButtons').click(function(){
	    	
	    		var id = $(this).attr("id");
	    		$.post('dispatcher?operation=addwish',{addWishID:id, addWishByUser:$('#username').val()},function(responseText) { 

	    			$('#'+id).addClass('disabled');  
           });

	      });
	    $(document).ready(function() {
	        $('#mytable').dataTable( {
	            "pagingType": "full_numbers"
	        } );
	    } );
	 });
</script>

</head>
<body>

<jsp:include page="/header.jsp"/>

<iframe name="hiddenFrame" class="hide"></iframe>

<c:choose>
<c:when test="${not empty items}">


<div class="container">
<div class="page-header">
<h2>Search Result</h2>
</div>

<table id="mytable" class="table">
<thead>
<tr><th></th>
	<th>Title</th>
	<th>Price</th>
	<th>EndTime</th>
	<th>Actions</th>
</tr>
<tbody>
<c:forEach var="item" items="${items}">
<tr><td><img src="${item.picture}" alt="${item.title}" width="220" height="280"></td>
	<td>${item.title}</td>
		<c:set var="string1" value="${item.currency}" />
		<c:choose>
			<c:when test="${string1 == 'AUD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'USD'}">
				<c:set var="cur" value="$" />
			</c:when>
			<c:when test="${string1 == 'GBP'}">
				<c:set var="cur" value="£" />
			</c:when>
			<c:otherwise>
				<c:set var="cur" value="€" />
			</c:otherwise>
		</c:choose>
	<td>${string1} <br>
		Best Bid: ${cur}${item.bestBid}<br>
		Bidding Increments: ${cur}${item.bidIncrements}
	</td>
	<td>End Time:  <fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${item.endTime}" /><br>
		Status: ${item.itemStatus}</td>
	<td>
		<c:choose>
			<c:when test="${item.itemStatus == 'LIVE'}">
			 	<form action="dispatcher?operation=itemInfo" method="POST" >
					<input type="hidden" name="itemID" value="${item.itemId}" />
					<input type="submit" class="btn btn-default" value="Bid Now"  />
				</form>
			</c:when>
			<c:otherwise>
				<form action="dispatcher?operation=itemInfo" method="POST" >
					<input type="hidden" name="itemID" value="${item.itemId}" />
					<input type="submit" class="btn btn-default" value="Show Info"  />
				</form>
			</c:otherwise>
		</c:choose>
		<br>
		<c:choose>
			<c:when test="${not empty user}">
				<c:set var="contains" value="false" />
				<c:forEach var="curItem" items="${wishlist}">
				  <c:if test="${curItem.itemId eq item.itemId}">
				    <c:set var="contains" value="true" />
				  </c:if>
				</c:forEach>
			 	<c:choose>
			 	<c:when test="${contains == 'true'}">
			 		<input type="hidden" id="username" value="${user.username}" />
					<button id="${item.itemId}" class="addButtons btn btn-default btn-sm" type="submit" disabled >Add to Wish List</button>
				</c:when>
				<c:otherwise>
					<input type="hidden" id="username" value="${user.username}" />
					<button id="${item.itemId}" class="addButtons btn btn-default btn-sm" type="submit" >Add to Wish List</button>
				</c:otherwise>
				</c:choose>
			</c:when>
		</c:choose>
	</td>
</c:forEach>
</tbody>
</table>

</div>

	
	
</c:when>
<c:otherwise>
<div class="container">
<div class="page-header">
<h2>Search Result</h2>
</div>
	<div>No matched item found.</div><br><br>
	<input type="button" class="btn btn-primary btn-lg" value="Back" onClick="window.history.back();"></div>
</c:otherwise>
</c:choose>


<jsp:include page="/footer.jsp"/>
</body>
</html>
