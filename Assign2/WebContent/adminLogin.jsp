<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script> 

<%@ include file="css/css.html" %>
<%@ include file="css/admin.html" %>

<title>Admin Login Page</title>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <br><br>
    <a href="forward.jsp"><img style="margin:0px auto;display:block; align: middle;" src="images/logo6.png" alt="Square Root - Online Auction" width="180" height="30"></a> 
  </div>
</nav>


<div class="container">
<h1 class="text-center " style='font-weight: 100;letter-spacing: 2px'>Welcome Back. Admin Sign in.</h1>

    <div class="row">
        <div class="col-md-6 col-md-offset-3"> 
            <h1 class="text-center login-title" style='font-weight: 100;letter-spacing: 2px'>Square Root admin only</h1>
            <c:choose>
				<c:when test="${not empty param.failed}">
					<h4 class="text-center "  style="color: red;"><span class="glyphicon glyphicon-exclamation-sign"></span> Your username or password is incorrect.</h4>
				</c:when>
			</c:choose>
            <div class="account-wall">
                <img class="profile-img" src="images/logo.png"
                    alt="">
                <form class="form-signin" action="dispatcher?operation=adminlogin" method="post">
	                <input type="text" name="username" class="form-control" placeholder="Username" required autofocus>
	                <input type="password" name="password" class="form-control" placeholder="Password" required>
	                <button style="background:#1C86EE;" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      				<br><br>
                </form>
            </div>
        </div>
    </div>
</div>

<jsp:include page="/footer.jsp"/>
</body>
</html>