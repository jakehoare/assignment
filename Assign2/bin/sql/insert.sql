insert into user values ('abc', 'password', 'a@b.c', 'cba', 'a', 'bc', '1980-02-02', '11 a st', 'Sydney','NSW','Australia', '2102', '1234567890123456', 'LIVE', 0);
insert into user values ('hola', 'blah', 'hello@c.com', 'ya', 'bbb', 'real', '1989-10-16', '7 king st', 'Foo', null,'Spain', '210231', '1234567890123457', 'LIVE', 0);
insert into user values ('john', 'youguess', 'john@csda.com', 'j', 'ohn', 'smith', '1990-11-06', '7 qs st', 'Houston', null,'US', '9891', '2234567890123457', 'BANNED', 0);
insert into user values ('yo', 'whatever', 'yo@ca.net', 'yo', 'hey', 'man', '1970-12-03', '7 sup st', 'Manchester', null,'UK', '90782231', '2434567890123457', 'PENDACK', 0);
insert into user (username, password, email) values ('admin', 'adminp', 'admin@a.com');


insert into item values (1, 'abc', 'MacBook Pro 2015', 'Computers', 'Cool notebook','http://cdn.cultofmac.com/wp-content/uploads/2014/03/old-MacBook-Pro-13-640x386.jpg', 'AUD', '1026.00', '10', '100.00', null, null, '2015-05-19 03:14:07', 'LIVE');
insert into item (seller, title, category, currency, reserve_price, bid_increments, best_bid, end_time, status)
	values ('abc', 'MacBook Pro 2008', 'Computers', 'AUD', '613.00', '12', '233.00', '2015-04-19 03:14:07', 'NOTSOLD');
insert into item (seller, title, category, description, picture, currency, reserve_price, bid_increments, best_bid, best_bid_time, best_bidder, end_time, status)
	values ('abc', 'Burberry Prorsum Dress', 'Clothing', 'Old dress, Tiered silk-organza dress','http://cache.net-a-porter.com/images/products/538741/538741_in_xl.jpg', 'USD', '5750.00', '120', '6000.00','2015-04-21 03:04:19', 'hola', '2015-04-21 03:04:19', 'SOLD');
insert into item (seller, title, category, description, picture, currency, reserve_price, bid_increments, best_bid, best_bid_time, best_bidder, end_time, status)
	values ('abc', 'Rupert Sanderson Point-toe Flats', 'Shoes', 'Old shoes, Bolero embellished leather point-toe flats','http://cache.net-a-porter.com/images/products/607775/607775_in_xl.jpg', 'USD', '475.00', '20', '300.00', null, null, '2015-04-25 13:23:12', 'PENDING');

	
insert into wishlist values ('john', 1);
insert into wishlist values ('hola', 3);