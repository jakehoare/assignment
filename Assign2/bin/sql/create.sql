GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON auction.* TO square@'localhost' IDENTIFIED BY 'root';
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE ON auction.* TO square@'%' IDENTIFIED BY 'root';


CREATE TABLE User (
	username	varchar(20) not null,
	password	char(64)	not null,
	email		varchar(255)	not null  unique  check (value like '%@%.%'),
	nickname	varchar(255),	-- default to username if not supplied
	first_name 	varchar(255),
	last_name 	varchar(255),
	date_of_birth	date,
	address_street	varchar(255),
	address_city	varchar(255),
	address_state	varchar(255),
	address_country	varchar(255),
	address_postcode	varchar(20),
	credit_card	char(16),
	status	ENUM('LIVE','BANNED','PENDACK'),
	is_admin tinyint(1) default 0,
	PRIMARY KEY (username)
);


CREATE TABLE Item (
	item_id		serial,
	seller		varchar(20)	not null,
	title		varchar(255)	not null,
	category	ENUM('Arts','Books','Clothing','Computers','Electronics','Games','Shoes','Sports')	not null,
	description	varchar(2048),
	picture		varchar(2048),
	currency	ENUM('AUD','GBP','EUR','USD')		not null, 	-- bid and reserve ccy must be the same
	reserve_price	decimal(10,2)	not null  check (value >= 0),
	bid_increments	decimal(10,2)	not null  check (value > 0),
	best_bid		decimal(10,2)	not null  check (value >= 0), -- initially set by seller bid start price
	best_bid_time	timestamp,
	best_bidder	varchar(20),
	end_time		timestamp	not null,
	status		ENUM('LIVE','SOLD','NOTSOLD','PENDING','HALT')	not null,
	PRIMARY KEY (item_id),
	FOREIGN KEY (seller) REFERENCES User(username),
	FOREIGN KEY (best_bidder) REFERENCES User(username)
);


CREATE TABLE Wishlist (
	username	varchar(20),
	item_id		BIGINT UNSIGNED,
	PRIMARY KEY (username, item_id),
	FOREIGN KEY (username) REFERENCES User(username),
	FOREIGN KEY (item_id) REFERENCES Item(item_id)
);	