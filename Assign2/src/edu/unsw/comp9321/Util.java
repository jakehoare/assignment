package edu.unsw.comp9321;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.DOMException;

public class Util {
  public static boolean isNullOrEmpty(String str) {
    return str == null || str.isEmpty() || str.trim().isEmpty();
  }
	
	public static BigDecimal parseToBigDecimal(String value) {
		try {
			if (value != null && !value.isEmpty())
				return new BigDecimal(value);
			return null;
		}
		catch (NumberFormatException e) {
			return null;
		}
	}
	
	public static Timestamp parseToTimestamp(String value, String format) {
		Timestamp timestamp = null;
	  if (value != null && !value.isEmpty()) {
		  try {
		  	SimpleDateFormat formatter = new SimpleDateFormat(format);
		    Date date = formatter.parse(value);
		    timestamp = new Timestamp(date.getTime());
		  } catch (DOMException e) {

		  } catch (ParseException e) {

		  } catch (IllegalArgumentException e) {
		  	
		  }
	  }
	  return timestamp;
  }
	
	public static Timestamp addMinutes(Timestamp timestamp, int minutes) {
	  if (timestamp != null) {
	  	Calendar c = Calendar.getInstance();
	  	c.setTime(timestamp);
	  	c.add(Calendar.MINUTE, minutes);
	  	timestamp = new Timestamp(c.getTime().getTime());
	  }
	  return timestamp;
  }
	
	public static Timestamp formatTimestamp(Timestamp timestamp, String format) {

	  if (timestamp != null) {
		  try {
		  	SimpleDateFormat formatter = new SimpleDateFormat(format);
		  	Date date = formatter.parse(timestamp.toString());
		  	timestamp = new Timestamp(date.getTime());
		  } catch (DOMException e) {

		  } catch (ParseException e) {

		  } catch (IllegalArgumentException e) {
		  	
		  }
	  }
	  return timestamp;
	}
	
	public static String parseDateToString(Date date, String format) {
		String dateInString = "";
	  if (date != null) {
		  try {
		  	SimpleDateFormat formatter = new SimpleDateFormat(format);
		  	dateInString = formatter.format(date);
		  } catch (DOMException e) {
		  } catch (IllegalArgumentException e) {	
		  }
	  }
	  return dateInString;
  }
}
