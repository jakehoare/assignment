package edu.unsw.comp9321;

import java.util.Date;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.*;

public class EmailUtil {
	
	// Send html message
	public static boolean sendEmail(String host, String port,
		      final String userName, final String password, String toAddress,
		      String subject, String message) {

				// sets SMTP server properties
		    Properties properties = new Properties();
		    properties.put("mail.smtp.host", host);
		    properties.put("mail.smtp.port", port);
		    properties.put("mail.smtp.auth", "true");
		    properties.put("mail.smtp.starttls.enable", "true");

		    // creates a new session with an authenticator
		    Authenticator auth = new Authenticator() {
		        public PasswordAuthentication getPasswordAuthentication() {
		            return new PasswordAuthentication(userName, password);
		        }
		    };

		    Session session = Session.getInstance(properties, auth);
		    
		    // creates a new e-mail message
		    Message msg = new MimeMessage(session);

		    try {
			    msg.setFrom(new InternetAddress(userName));
			    InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
			    msg.setRecipients(Message.RecipientType.TO, toAddresses);
			    msg.setSubject(subject);
			    msg.setSentDate(new Date());
			    msg.setContent(message, "text/html; charset=utf-8");

			    // sends the e-mail
			    Transport.send(msg);
			    
			    System.out.println("Sent message successfully....");
			    return true;
		    } catch (AddressException e) {
			    e.printStackTrace();
			    return false;
		    } catch (MessagingException e) {
			    e.printStackTrace();
			    return false;
		    }
	}
}
